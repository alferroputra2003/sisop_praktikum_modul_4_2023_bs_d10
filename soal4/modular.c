#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h> 

bool isFlag(int n)
{
    bool flag = true;
 
    // Corner case
    if (n <= 1)
        return false;
    
    return flag;
}

int main(){

    char logReport[] = 'Report';
    char logFlag[] = 'Flag';
    int check = 0;

    if (isFlag(check)){
        printf("%s\n", logReport);
    }
    else{
        printf("%s\n", logFlag);
    }

    return 0;
}