# sisop_praktikum_modul_4_2023_BS_D10

## Nomor 1
### Storage.c

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>

// int main() {
//     system("awk -F\",\" '$3 < 25' 'FIFA23_official_data.csv' | awk -F\",\" '$8 > 85' | awk -F\",\" '$9 != \"Manchester City\" {print $2 \",\" $9 \",\" $3 \",\" $5 \",\" $8 \",\" $4}'");

// }

// awk -F',' '$3 < 25' 'FIFA23_official_data.csv'|awk -F',' '$8>85'|awk -F',' '$9!="Manchester City" {print $2 "," $9 "," $3 "," $5 "," $8 "," $4}'

int main() {
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {

        pid_t pid2 = fork();

        if(pid2>0)
        {
            int status;
            wait(&status);
            printf("\nparent2\n");
            char *args[]= {"unzip", "fifa-player-stats-database.zip", NULL};
            execv("/bin/unzip", args);
        }
        else if (pid2 == 0)
        {
            printf("\nchild2\n");
            system("kaggle datasets download -d bryanb/fifa-player-stats-database");
        }
    }
    else {
        int status;
        wait(&status);
        printf("\nparent1\n");
        system("awk -F\",\" '$3 < 25' 'FIFA23_official_data.csv' | awk -F\",\" '$8 > 85' | awk -F\",\" '$9 != \"Manchester City\" {print $2 \",\" $9 \",\" $3 \",\" $5 \",\" $8 \",\" $4}'");
        
        exit(0);
    }
}
```

Pada bagian ini, saya menggunakan forking dengan pembagian pada child 2(yang dijalankan pertama) akan melakukan download file, pada parent 2 (yang dijalankan kedua) akan melakukan unzip menggunakan command execv, dan terakhir pada parent 1(yang dijalankan terakhir) akan melakukan seleksi data pada file FIFA23_official_data.csv dengan ketentuan, potensial lebih dari 85, umur kurang dari 25 dan tidak berasal dari tim Manchester City.

### Dockerfile

```
FROM ubuntu:latest

RUN apt-get update && apt-get install -y python3 python3-pip

RUN pip3 install kaggle

COPY storage.c .

RUN mkdir -p /root/.kaggle

COPY --chmod=600 kaggle.json /root/.kaggle/kaggle.json

ENV PATH=/home/parallels/.local/bin${PATH:+:${PATH}}

RUN apt-get install -y unzip


RUN gcc storage.c -o storage

CMD ["./storage"]
```

Pada bagian ini kita mengambil FROM terhadap ubuntu terbaru/latest. kemudian kita lakukan running untuk update dan juga menginstall command python berupa pip. kemudian meggunakan pip kita akan melakukan instalasi kaggle. kemudian kita melakukan COPY storage.c, dilanjutkan dengan pembuatan direktori .kaggle pada root. kemudian kita menyalik isi kaggle.json ke direktori yang kita buat tadi. Sekalian juga kita lakukan pembatasan akses menggunakan chmod. langkah selanjutnya adalah menyatakan path. Kemudian agar execv dapat berjalan kita melakukan instalasi unzip. kemudian kita RUN gcc storage.c -o storage agar bisa dijalankan. dan terakhir kita bash storage tersebut.

### Docker-compose.yml

```
version: '3'
services:
  storage:
    image: yudisthiraputra/soal1praktikum4
    command: ["./storage"]
    deploy:
      replicas: 5
    ports:
    - "8100-8104:8000"

```

kita nyatakan version sebagai 3, kemudian kita dapatkan image dari dockerhub user yudisthiraputra dengan repository bernamakan soal1praktikum4. command yang akan dijalankan adalah ./storage, karena diminta jumlah instances sebesar 5 maka kita nyatakan replica sejumlah 5. Kemudian kita nyatakan ports sejumlah 5 makanya terdapat rentang 8100-8104.
