#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>

// int main() {
//     system("awk -F\",\" '$3 < 25' 'FIFA23_official_data.csv' | awk -F\",\" '$8 > 85' | awk -F\",\" '$9 != \"Manchester City\" {print $2 \",\" $9 \",\" $3 \",\" $5 \",\" $8 \",\" $4}'");

// }

// awk -F',' '$3 < 25' 'FIFA23_official_data.csv'|awk -F',' '$8>85'|awk -F',' '$9!="Manchester City" {print $2 "," $9 "," $3 "," $5 "," $8 "," $4}'

int main() {
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {

        pid_t pid2 = fork();

        if(pid2>0)
        {
            int status;
            wait(&status);
            printf("\nparent2\n");
            char *args[]= {"unzip", "fifa-player-stats-database.zip", NULL};
            execv("/bin/unzip", args);
        }
        else if (pid2 == 0)
        {
            printf("\nchild2\n");
            system("kaggle datasets download -d bryanb/fifa-player-stats-database");
        }
    }
    else {
        int status;
        wait(&status);
        printf("\nparent1\n");
        system("awk -F\",\" '$3 < 25' 'FIFA23_official_data.csv' | awk -F\",\" '$8 > 85' | awk -F\",\" '$9 != \"Manchester City\" {print $2 \",\" $9 \",\" $3 \",\" $5 \",\" $8 \",\" $4}'");
        
        exit(0);
    }
}